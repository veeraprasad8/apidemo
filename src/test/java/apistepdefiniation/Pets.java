package apistepdefiniation;

import com.fasterxml.jackson.databind.util.BeanUtil;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import gherkin.deps.com.google.gson.Gson;
import gherkin.deps.com.google.gson.reflect.TypeToken;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.pet.pojo.PetPojo;
import org.pet.pojo.Tag;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;

public class Pets {
    Response response;

    @Given("^User given all the inputs parameters for pet get api$")
    public void user_given_all_the_inputs_parameters_for_pet_get_api() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        response = RestAssured.given().get("https://petstore.swagger.io/v2/pet/1");
    }

    @When("^User calling an petstore pet get api$")
    public void user_calling_an_petstore_pet_get_api() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        response.prettyPrint();
    }

    @Then("^User should be able to get the pet response$")
    public void user_should_be_able_to_get_the_pet_response()  {
        // Write code here that turns the phrase above into concrete actions
        JsonPath js = new JsonPath(response.asString());
       // System.out.println(js.get("tags"));
      //  ArrayList al = js.getJsonObject("tags");
      //  System.out.println(al.get(0));

        PetPojo pt = new Gson().fromJson(response.asString(), PetPojo.class);
        System.out.println("name of pet is :"+pt.getName());
       // Tag ts = new Gson().fromJson(pt.getTags(), Tag.class);


       // List<Tag> ts =new Gson().fromJson(pt.getTags().toString(), new TypeToken<ArrayList<Tag>>(){}.getType());
        //pt.setTags(ts);

        System.out.println(pt.getTags().get(0).getId());



       // response.then().assertThat().body(matchesJsonSchema("C:\\Users\\Sai Ram\\workspace\\ApiAuto\\src\\test\\java\\resources\\EmployeeApis\\pets.json"));

    }

}
