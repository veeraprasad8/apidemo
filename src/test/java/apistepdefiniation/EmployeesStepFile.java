package apistepdefiniation;


import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import org.com.epam.base.ApiEvent;
import org.com.epam.base.MyEpamlBase;
import org.junit.Assert;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;
import static io.restassured.RestAssured.given;


public class EmployeesStepFile extends MyEpamlBase {

    public Response empJsonResponse;
    public String empApi = ApiEvent.EMPLOYEE_API;
    @Given("^User provided all the inputs parameters for an api$")
    public void user_provided_all_the_inputs_parameters_for_an_api() throws Exception {
        // Write code here that turns the phrase above into concrete actions
       empJsonResponse =  RestAssured.given().spec(setParams()).contentType(ContentType.JSON).when().get(empApi);



    }

    @When("^User hitting an employee get api$")
    public void user_hitting_an_employee_get_api() throws Exception {
        // Write code here that turns the phrase above into concrete actions
        System.out.println("this is response api method");
        empJsonResponse.prettyPrint();
        empJsonResponse.getContentType();
    }

    @Then("^User should be able to get all the employee information and response schema also validated$")
    public void user_should_be_able_to_get_all_the_employee_information_and_response_schema_also_validated() throws Exception {
        // Write code here that turns the phrase above into concrete actions
        System.out.println("Validating the status code and response time");
       Assert.assertTrue(statusChecks(empJsonResponse));
        System.out.println(empJsonResponse.getHeader("cache-control"));
        ResponseBody body = empJsonResponse.getBody();
        System.out.println("response body is :"+body.asString());
     //   System.out.println("Validating the Employee response json schema validating");
       // System.out.println(System.getProperty("user.dir"));
       // empJsonResponse.then().assertThat().body(matchesJsonSchema("C:\\Users\\Sai Ram\\workspace\\ApiAuto\\src\\test\\java\\resources\\EmployeeApis\\GetAllEmployee.json"));

       /* JsonPath jso = empJsonResponse.jsonPath();
        System.out.println(jso.get("id"));
        JsonPath js = new JsonPath(empJsonResponse.asString());
        System.out.println(js.get("id"));*/


    }

}
