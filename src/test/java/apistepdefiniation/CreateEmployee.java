package apistepdefiniation;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.com.epam.base.ApiEvent;
import org.com.epam.base.MyEpamlBase;
import org.junit.Assert;

import static io.restassured.RestAssured.when;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;
import static io.restassured.RestAssured.given;

public class CreateEmployee extends MyEpamlBase {

    public Response empJsonResponse;
    public String empPostApi = ApiEvent.EMPLOYEE_CREATION_API;
    @Given("^User provided all the inputs parameters for a post api$")
    public void user_provided_all_the_inputs_parameters_for_a_post_api()  {
        // Write code here that turns the phrase above into concrete actions

        System.out.println("this is parameters of post api");
    }

    @When("^User Providing payload json and hitting post api$")
    public void user_Providing_payload_json_and_hitting_post_api(String empJson)  {
        // Write code here that turns the phrase above into concrete actions
        System.out.println("payload is :"+empJson);

       empJsonResponse = RestAssured.given().spec(setParams()).body(empJson).post(empPostApi);
    }

    @Then("^User should be able to get the new employee json response$")
    public void user_should_be_able_to_get_the_new_employee_json_response()  {
        // Write code here that turns the phrase above into concrete actions

        empJsonResponse.prettyPrint();
        System.out.println("employee post api status code :"+empJsonResponse.statusCode());
        Assert.assertTrue(statusChecks(empJsonResponse));
        System.out.println(empJsonResponse.getHeader("cache-control"));
        //empJsonResponse.then().assertThat().body(matchesJsonSchema("C:\\Users\\Sai Ram\\workspace\\ApiAuto\\src\\test\\java\\resources\\EmployeeApis\\PostEmployeeCreation.json"));
       // JsonPath js = new JsonPath(empJsonResponse.asString());
        //System.out.println(empJsonResponse.path("name"));
       // System.out.println(js.get("name"));

    }
}
