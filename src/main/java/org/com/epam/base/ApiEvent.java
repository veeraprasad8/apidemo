package org.com.epam.base;

public interface ApiEvent {

    String EMPLOYEE_API = "employees"; //get api
    String EMPLOYEE_CREATION_API = "create" ; //Post api
}
