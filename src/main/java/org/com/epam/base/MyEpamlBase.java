package org.com.epam.base;


import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.JSONObject;
import org.testng.annotations.BeforeSuite;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;

public class MyEpamlBase {


   public String Authorizationvalue;
    static Properties prop = null;
    static Properties properties = null;


    public Boolean comparitiveTestingRequired = true;



    public void loadingPropertyFile(){
        String file_name = null;
        String env_name = System.getProperty("ENVIRONMENT", "STAGING");
        properties = new Properties();
        FileInputStream inputStream;
        try {
            if (env_name.equalsIgnoreCase("PROD"))
                file_name = "C:\\Users\\Sai Ram\\workspace\\ApiAuto\\src\\main\\java\\org\\api\\utility\\config\\config_preprod.properties";
            else if (env_name.equalsIgnoreCase("\\PREPROD"))
                file_name = System.getProperty("user.dir") + "\\config\\config_preprod.properties";
            else if (env_name.equalsIgnoreCase("STAGING"))
                file_name = "C:\\Users\\Sai Ram\\workspace\\ApiAuto\\src\\main\\java\\org\\api\\utility\\config\\config_staging.properties";
            else file_name = System.getProperty("user.dir") + "\\config\\config_test.properties";
            inputStream = new FileInputStream(file_name);
            properties.load(inputStream);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /*
    /this function for mongo connection
    */

   // @AfterSuite
     /*
    /this function for mongo connection close
    */

     /*
    /this function for stausCode check and response time
    */

    public Boolean statusChecks(Response response) {

        if (response.statusCode() == 200) {
            if (response.getTimeIn(TimeUnit.SECONDS) < 60) {
                System.out.println("Response Time is less than 60 i.e: " + response.getTimeIn(TimeUnit.SECONDS) + " Second(s)");
                return true;
            } else {
                System.out.println("Response time is greater than 60 i.e " + response.getTimeIn(TimeUnit.SECONDS) + " Second(s)");
                return false;
            }
        } else if (response.statusCode() == 401) {
            System.out.println("User is Unauthorised" + response.statusCode());
            return false;
        } else if (response.statusCode() == 502) {
            System.out.println("Bad Gateway" + response.statusCode());
            return false;
        }  else if (response.statusCode() == 400){
                System.out.println("Bad Request"+response.statusCode());
            return false;
        } else {
            System.out.println("Unable to Call the API " + response.statusCode());
            return false;
        }
    }

     /*
    /this function for building request
    */

    public RequestSpecification setParams() {
        configure("EPAM");

        return getRequestSpecification(setHeaders());
    }

    public RequestSpecification getRequestSpecification(Map<String, String> headers) {

        return RestAssured.given().log().all().
               headers(headers).contentType(ContentType.JSON);
    }

    public File getTestDataClassPath(String file_name) {
        String pack = this.getClass().getPackage().getName();
        pack = pack.replaceAll("\\.", Matcher.quoteReplacement(File.separator));
        String pathname = "src" + File.separator + "test" + File.separator + "resources" + File.separator + "test_data"
                + File.separator + pack;
        File file = new File(System.getProperty("user.dir") + File.separator + pathname + File.separator + file_name);
        return file;
    }


    /*
   /this function for get todaydate
   */
    public String getDate(String format) {
        SimpleDateFormat sDate = new SimpleDateFormat(format);
        Date now = new Date();
        String todaydate = sDate.format(now);
        return todaydate;
    }



    public static boolean booleanValue(String propName, boolean defaultValue) {
        String prop = System.getProperty(propName);
        boolean val;

        if (prop == null)
            val = defaultValue;
        else
            val = Boolean.valueOf(prop.trim());

        return val;
    }

     /*
    /this function for configuring api hostname and url
    */

    public void configure(String key_start_name) {
        loadingPropertyFile();
        RestAssured.reset();
        Set<String> keys = properties.stringPropertyNames();
        System.out.println(keys);
        for (String key : keys) {
            if (key.startsWith(key_start_name) && key.endsWith("IP"))
                RestAssured.baseURI = properties.getProperty(key);
            else if (key.startsWith(key_start_name) && key.endsWith("PORT"))
                RestAssured.port = Integer.parseInt(properties.getProperty(key));
            else if (key.startsWith(key_start_name) && key.endsWith("BASE_PATH"))
                RestAssured.basePath = properties.getProperty(key);
        }
    }



    public Map<String, String> setHeaders() {

     Map<String, String> headers = new HashMap<String, String>();
        /*headers.put("Authorization", "Bearer " + PostUserLogin.getInstance().getToken());
        System.out.println("Authorizarion token is :"+PostUserLogin.getInstance().getToken());
        System.out.println("Headers are :" + headers);*/
        headers.put("Content-Type","application/json");
        return headers;
    }



   /* public Map<String, String> setQueryParam() {

        Map<String, String> queryParams = new HashMap<String, String>();
       queryParams.put("os", "ALL");
       queryParams.put("minandroid", "ALL");
       queryParams.put("maxandroid", "ALL");
       queryParams.put("minios", "ALL");
       queryParams.put("maxios", "ALL");
        return queryParams;
    }
*/

    // this method is post api to login in to cms application to get the token header value using singleton pattern.
 /*   @BeforeTest
    public void validateLoginCMS() throws Exception {
        Response JsonResponse;
        //RequestSpecification Request = RestAssured.given();

        JSONObject obj = new JSONObject();
        obj.put("password", "prasadtester")//userone
                .put("username", "prasadnv");//userone
        System.out.println("Entering into base before test");
        configure("CMS");
        String value = obj.toString();
        // RestAssured.proxy = ProxySpecification.host( "northproxy.airtelworld.in")
        //  .withPort(4145).withAuth("a1n23n8h","Bmw@2018");
        JsonResponse = RestAssured.given().contentType(ContentType.JSON).body(value).post(API);
        String sbody = JsonResponse.asString();
        System.out.println("json response string is :"+sbody);
        ResponseObj responseObj = new Gson().fromJson(sbody,ResponseObj.class);
        PostUserLogin postUserLogin
                = new Gson().fromJson(responseObj.getObject().toString(), PostUserLogin.class);
        PostUserLogin.getInstance().setToken(postUserLogin.getToken());
        System.out.println("postUserLogin.toString() :: " + postUserLogin.toString());
         Authorizationvalue = postUserLogin.getToken();

    }*/

     /*
    /this function for loading the properties file and getting the values based on keys.
    */

    public static String setPropFile(String Key) throws IOException {
        prop = new Properties();
        File f = new File(System.getProperty("user.dir") + "/config/server.properties");
        FileReader reader = new FileReader(f);
        prop.load(reader);
        return prop.get(Key).toString();

    }

}